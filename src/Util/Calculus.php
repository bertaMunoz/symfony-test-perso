<?php

namespace App\Util;

class Calculus
{

  public function result($a, $b, $operator)
  {

    if (!is_numeric($a) || !is_numeric($b)) {
      throw new \Exception("non numeric values");
    }

    switch ($operator) {
      case "+":
        return $a + $b;
      case "-":
        return $a - $b;
      case "*":
        return $a * $b;
      case "/":
        return $a / $b;
    }
  }
}