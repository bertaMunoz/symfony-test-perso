<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Form\BookType;
use App\Entity\Book;

class AddBookController extends Controller
{
    /**
     * @Route("/book", name="book")
     */
    public function index(Request $req)
    {

        $book = new Book(); //avec ou dans instance de book
     
        $form= $this->createForm(BookType::class, $book);

        $form->handleRequest($req);

        if($form->isSubmitted() && $form->isValid()){

            $book = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($book);
            $em->flush();
        }

        return $this->render('book/index.html.twig', [
            'form'=>$form->createView(),
        ]);
    }
}
