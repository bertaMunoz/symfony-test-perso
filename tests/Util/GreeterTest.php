<?php

namespace App\Test\Util;

use PHPUnit\Framework\TestCase;
use App\Util\Greeter;

class GreeterTest extends TestCase{

  private $greeter;  // ici on fait une propriété dans laquelle mettre la déclaration de nouvelle instance:>  $this->greeter = new Greeter();

  public function setUp(){// méthode qui sera déclenchée avant chaque test et remettra à zéro avant chaque test. chaque test utilisera une instance fraiche de greeter. 
      $this->greeter = new Greeter(); // on déclare ici une nouvelle instance qui servira pour tous les tests. 
  }

  public function testFirst(){
    $this->assertEquals("bloup", "bloup");
    $this->assertFalse(false); 
  }

  public function testGreetSuccess(){
   // $greeter = new Greeter(); //on fait une nouvelle instance de la classe qu'on a à tester. 
    $result = $this->greeter->greet("bloup");// déclenche la methode en mettant un argument et récupère le résultat de la methode en mettant une variable // bloup remplace le $name qui est dans Greeter.php
    $expect = "Hello bloup, how's it going?"; // definie le résultat attendu avec cette methode

    $this->assertEquals($expect, $result);
  }

  public function testGreetNumber(){
    //$greeter = new Greeter();

    $result = $this->greeter->greet(45);
    $expect = "Hello 45, how's it going?";

    $this->assertEquals($expect, $result);
  }

  /**
   * @expectedException TypeError 
   */
  public function testGreetTypeException(){
    $this->greeter->greet([]);
  }
}