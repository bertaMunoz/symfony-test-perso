<?php

namespace App\Test\Util;

use PHPUnit\Framework\TestCase;
use App\Util\Calculus;


class CalculusTest extends TestCase{

  private $calculus; 

  public function setUp(){

    $this->calculus = new Calculus(); 

  }

  // public function testCalculusAdd(){

  //   $result = $this->calculus->result(1,1,"+");
    
  //   $expect = 2;
    
  //   $this->assertEquals($expect, $result);
  // }

/**
 * @dataProvider successProvider
 */
   public function testCalculusAdd($a, $b, $operator, $expect){ // on lui passe des arguments qui vaudront ce que l'on va donner dans le successProvider : ex=> pour le + => $a = 1, $b = 1, $operator = "+", et $expect = 2, et ainsi de suite pour les autres (le -, le *, le/). 
    
    
    $result = $this->calculus->result($a,$b,$operator);
      
    $this->assertEquals($expect, $result);
  }
  public function successProvider(){
    return [
      [1,1, "+", 2],
      [1,1, "-", 0],
      [1,1, "*", 1],
      [4,2, "/", 2],
      [1.1, 2.1, "+", 3.2]

    ];
  }

  /**
   * @expectedException Exception
   */
  public function testResultTypError(){

    $this->calculus->result("bloup", "blip", "+");
    
  }
}