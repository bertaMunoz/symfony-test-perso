<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\ORM\Tools\SchemaTool;
use App\Entity\Book;

class AddBookControllerTest extends WebTestCase
{
    // public function testSomething()

    public function setUp(){

        $client = static::createClient();
        $manager = $client->getContainer()->get("doctrine")->getManager(); // on doit chopper d'abbord le container de symfony, puis sur ce container on récupère doctrine, puis le manager comme d'hab // en gros c'est la facon de récup l'entity manager depuis un test
        $schema = new SchemaTool($manager); // instance de schema tool
        $schema->dropDatabase();
        $schema->updateSchema($manager->getMetadataFactory()->getAllMetadata());// je récupère la sctructure de la base de données, je la donne à manger au shema (updateSchema) pour qu'il refasse la bdd.
    }

    public function testPageworking()
    {
        // $client = static::createClient(); 
        $client = static::createClient();

        // $crawler = $client->request('GET', '/');
        $crawler = $client->request('GET', '/book');

        // $this->assertSame(200, $client->getResponse()->getStatusCode());
        // $this->assertSame(1, $crawler->filter('h1:contains("Hello World")')->count());

        $this->assertSame(200, $client->getResponse()->getStatusCode()); //1ère assertion, est ce que la reponse a comme statut 200 => ok après notre 1ere assertion, est ce que la route fonctionne
       // $this->assertSame(1, $crawler->filter('h1:contains("Hello World")')->count()); //utilise le crawler et essaie d'aller chercher le h1 qui contient un h1 qui contient hello world => ici c'est un ex, nous on n'a pas de h1. 

       $this->assertSame(1, $crawler->filter("form")->count());//est ce qu'il y a bien un formulaire, 
       $this->assertSame(4, $crawler->filter("input")->count());//est ce qu'il y a bien un formulaire, 


    }
    public function testBookAdding()
    {
        // $client = static::createClient(); 
        $client = static::createClient();

        // $crawler = $client->request('GET', '/');
        $crawler = $client->request('GET', '/book');

        $form = $crawler->selectButton("Add")->form();
        $form["book[title]"] = "Test Titre";
        $form["book[content]"] = "Test contenu";
        $form["book[author]"] = "Test Auteur";
        $form["book[pageNumber]"] =  543;

        $client->submit($form);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $repo = $client->getContainer()->get('doctrine')->getRepository(Book::class); //on récupère le repo du livre et on dde au find all de nous renvoyer un seul élément (car on n'a qu'un élément, et on lui dde avec le 1 )
        $this->assertCount(1, $repo->findAll());
    }


}
